const array = [50, 70, 20, 19, 88, 108];

function search(arr, target) {
	const mid = Math.floor((arr.length - 1) / 2);
	let left = mid;
	let right = mid + 1;

	while (right - left >= 0) {
		if (target === arr[left] || target === arr[right]) return true;
		if (left > 0) left--;
		if (right < arr.length - 1) right++;
	}

	return false;
}

console.log(search(array, 50));

// Оптимизация поиск от середины в две стороны
