const array = [50, 70, 20, 50, 88, 50];

const countInArray = (arr, target) =>
	arr.reduce((acc, item) => (acc += target === item ? 1 : 0), 0);

console.log(countInArray(array, 50));

// Вернуть количество совпадений в массиве
