const array = [50, 70, 20, 19, 88, 108];

function search(arr, target) {
	let left = 0;
	let right = arr.length - 1;

	while (left <= right) {
		if (arr[left] === target || arr[right] === target) return true;

		left++;
		right--;
	}

	return false;
}

console.log(search(array, 108));

// Оптимизация поиск с двух сторон
