const array = [10, 20, 30, 40, 50];

function isSorted(arr, isDesc) {
	let index = arr.length - 1;

	while (index > 0) {
		if (isDesc && arr[index] < arr[index - 1]) return false;
		if (!isDesc && arr[index] > arr[index - 1]) return false;
		index--;
	}
	return true;
}

console.log(isSorted(array, false));

// Проверить массив является ли он отсортированным(в порядке возрастания/убывания)
