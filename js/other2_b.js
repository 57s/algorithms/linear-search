const array = [10, 20, 30, 40, 50];

console.log(array);

function isSorted(arr, isAscending) {
	for (let index = 0; index <= arr.length; index++) {
		if (
			(isAscending && arr[index] > arr[index + 1]) ||
			(!isAscending && arr[index] < arr[index + 1])
		)
			return false;
	}

	return true;
}

console.log(isSorted(array, true));

// Проверить массив является ли он отсортированным(в порядке возрастания/убывания)
