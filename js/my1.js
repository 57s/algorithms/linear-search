const array = [50, 70, 20, 19, 88, 108];

function search(arr, target) {
	for (let i = arr.length - 1; i >= 0; i--) {
		if (target === arr[i]) return i;
	}

	return false;
}

console.log(search(array, 108));
